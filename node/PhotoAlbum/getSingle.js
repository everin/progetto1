// MySQL
var mysql = require('mysql');

// Crea connessione al nostro database MySQL
function getMySQLConnection() {
	return mysql.createConnection({
		host: process.env.RDS_HOSTNAME,
		port: process.env.RDS_PORT,
		user: process.env.RDS_USERNAME,
		password: process.env.RDS_PASSWORD,
		database: process.env.RDS_DB_NAME
	});
}

exports.handler = (event, context, callback) => {
	var photoList = [];
	var response = {};
	
	console.log('Prova LOG getSingle');
	
	// Connessione al database MySQL
	var connection = getMySQLConnection();
	connection.connect();

    connection.query('select id, username, description from PhotoAlbum where id = ?', [event['pathParameters']['id']], function(error, rows, fields) {
	  	if (error) {
			// Alimento la risposta negativa
			response.statusCode = error.statusCode || 500;
			response.headers = { 'Content-Type': 'text/plain' };
			response.body = 'Internal server error';
			
			// Invio del messaggio negativo
			callback(error, response);
	  	} else {
	  		// Ciclo i risultati ottenuti
	  		for (var i = 0; i < rows.length; i++) {

	  			// Per ogni foto creo un oggetto che descrive il record in tabella
		  		var photoItem = {
		  			'id':rows[i].id
		  			, 'username':rows[i].username
		  			, 'description':rows[i].description
		  			//, 'photo':rows[i].photo
		  		}
		  		// Aggiungo l'oggetto realizzato alla lista
		  		photoList.push(photoItem);
			}

			// Alimento la risposta positiva
			response.statusCode = 200;
			response.headers = { 'Content-Type': 'application/json' };
			response.body = JSON.stringify(photoList);
			
			// Invio del messaggio positivo
			callback(null, response);
		}
	});

	// Chiude la connessione al database MySQL
	connection.end();
};